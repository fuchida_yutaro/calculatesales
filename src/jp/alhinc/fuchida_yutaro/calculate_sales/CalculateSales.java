
package jp.alhinc.fuchida_yutaro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {

//		コマンドライン引数チェック
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

//		Mapの作成
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();

//		支店定義ファイル読み込み
		if(!fileRead(args[0], "branch.lst", "^[0-9]{3}$", "支店", nameMap, salesMap)) {
			return;
		}

//		売り上げファイル読み込み
		BufferedReader br = null;

//      ファイル一覧を取得する
		File dir = new File(args[0]);
		File[] list = dir.listFiles();

//      該当するファイルのリスト
		ArrayList<File> fileList = new ArrayList<File>();

//      ファイル名のチェック
		for(int i = 0; i < list.length; i++) {
			if(list[i].getName().matches("^[0-9]{8}(.rcd)$") && list[i].isFile()) {
				fileList.add(list[i]);
			}
		}

//		連番チェック
		for(int i = 1; i < fileList.size(); i++) {
			int file1 = Integer.parseInt(fileList.get(i - 1).getName().replaceAll("[^0-9]", ""));
			int file2 = Integer.parseInt(fileList.get(i).getName().replaceAll("[^0-9]", ""));

			if(file2 - file1 != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(File file : fileList) {
			try {
//				ファイルの読み込み
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				ArrayList<String> data = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					data.add(line);
				}

//				行数チェック
				if(data.size() != 2) {
					System.out.println(file.getName() + "のフォーマットが不正です");
					return;
				}

//				支店コード
				String key = data.get(0);

//				salesMapに該当するkeyのチェック
				if(!salesMap.containsKey(key)) {
					System.out.println(file.getName() + "の支店コードが不正です");
					return;
				}

//				売上金額フォーマットチェック
				try {
					Long.parseLong(data.get(1));
				} catch (Exception e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

//				合計金額
				long sum = salesMap.get(key) +  Long.parseLong(data.get(1));

//				合計金額 桁数チェック
				if(String.valueOf(sum).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				salesMap.put(key, sum);

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

//		集計結果出力
		if(!fileWrite(args[0], "branch.out", nameMap, salesMap)) {
			return;
		}
	}

	static boolean fileRead(String filePath, String fileName, String format, String contents, HashMap<String, String> nameMap, HashMap<String, Long> salesMap) {
		BufferedReader br = null;
		try {
			File file = new File(filePath, fileName);

//			ファイルの存在チェック
			if(!file.exists()) {
				System.out.println(contents + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) != null) {
//				文字列の分割
				String[] names = line.split(",");

//				フォーマットチェック
				boolean code = !names[0].matches(format);
				if(code || names.length != 2) {
					System.out.println(contents + "定義ファイルのフォーマットが不正です");
					return false;
				}

//				mapに保存
				nameMap.put(names[0], names[1]);
				salesMap.put(names[0], (long)0);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	static boolean fileWrite(String filePath, String fileName, HashMap<String, String> nameMap, HashMap<String, Long> salesMap) {
		BufferedWriter bw = null;
		try {
			File file = new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

//			List 生成 (ソート用)
//	        List<Map.Entry<String, Long>> entries = new ArrayList<Map.Entry<String, Long>>(salesMap.entrySet());
//	        Collections.sort(entries, new Comparator<Map.Entry<String, Long>>() {
//
//	            @Override
//	            public int compare(Entry<String, Long> entry1, Entry<String, Long> entry2) {
//	                return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
//	            }
//	        });
//	        金額の降順に出力
//	        for (Entry<String, Long> s : entries) {
//	        	bw.write(s.getKey() + "," + nameMap.get(s.getKey()) + "," + s.getValue());
//	        	bw.newLine();
//	        }

//        	1行ずつ出力
			for(String key : nameMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}


